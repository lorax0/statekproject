﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine.TestTools;
using Statek.Player.Inventory;
using System.Linq;

namespace Tests.EditMode.Inventory
{
    public class InventoryTest
    {
        [Test]
        public void CanAddItemToEmptySlotTest()
        {
            //Arrange
            var inventory = this.GetEmptyInventory();
            var item = Substitute.For<Item>();

            //Act
            item.Name.Returns("Trash");
            inventory.AddItem(item);

            var expectedItem = item;
            var itemInSlot = inventory.Slots.ElementAt(0).GetItem();

            //Assert
            Assert.AreEqual(expectedItem, itemInSlot);
        }

        private Statek.Player.Inventory.Inventory GetEmptyInventory()
        {
            Statek.Player.Inventory.Inventory inventory = new Statek.Player.Inventory.Inventory();
            List<Slot> slots = this.GetEmptySlots();
            inventory.Slots = slots;
            return inventory;
        }

        private List<Slot> GetEmptySlots()
        {
            List<Slot> equipmentSlots = new List<Slot>();
            for (var i = 0; i < 5; i++)
            {
                var slot = new Slot();
                equipmentSlots.Add(slot);
            }
            return equipmentSlots;
        }

    }
}
