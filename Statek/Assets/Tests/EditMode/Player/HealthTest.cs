﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Statek.Health;
using Statek.Research;
using NSubstitute;

namespace Tests.EditMode.Player
{
    public class HealthTest
    {
        [Test]
        public void DamageMoreThenArmorMinusHealthTest()
        {
            //Arange
            var damage = 30;
            var health = 100f;
            var armor = 20f;
            var maxArmorStatistic = ScriptableObject.CreateInstance<ResearchableStatistic>();
            var maxHealthStatistic = ScriptableObject.CreateInstance<ResearchableStatistic>();

            HealthService healthService = new HealthService();
            maxArmorStatistic.Statistic = armor;
            maxHealthStatistic.Statistic = health;
            healthService.MaxArmor = maxArmorStatistic;
            healthService.MaxHealth = maxHealthStatistic;
            healthService.AddHealth(health);
            healthService.AddArmor(armor);
            //Act
            healthService.TakeDamage(damage);
            var expectedHealth = 90f;
            var expectedArmor = 0f;

            //Assert
            Assert.AreEqual(expectedHealth, healthService.Health);
            Assert.AreEqual(expectedArmor, healthService.Armor);
        }

        [Test]
        public void DamageLessArmorTest()
        {
            //Arange
            var damage = 30;
            var health = 100f;
            var armor = 50f;
            var maxArmorStatistic = ScriptableObject.CreateInstance<ResearchableStatistic>();
            var maxHealthStatistic = ScriptableObject.CreateInstance<ResearchableStatistic>();

            HealthService healthService = new HealthService();
            maxArmorStatistic.Statistic = armor;
            maxHealthStatistic.Statistic = health;
            healthService.MaxArmor = maxArmorStatistic;
            healthService.MaxHealth = maxHealthStatistic;
            healthService.AddHealth(health);
            healthService.AddArmor(armor);
            //Act
            healthService.TakeDamage(damage);
            var expectedHealth = health;
            var expectedArmor = 20f;

            //Assert
            Assert.AreEqual(expectedHealth, healthService.Health);
            Assert.AreEqual(expectedArmor, healthService.Armor);
        }

        [Test]
        public void AreDeadWhenHealthAreLessThanZero()
        {
            //Arange
            var damage = 50;
            var health = 20f;
            var armor = 20f;
            var maxArmorStatistic = ScriptableObject.CreateInstance<ResearchableStatistic>();
            var maxHealthStatistic = ScriptableObject.CreateInstance<ResearchableStatistic>();

            HealthService healthService = new HealthService();
            maxArmorStatistic.Statistic = armor;
            maxHealthStatistic.Statistic = health;
            healthService.MaxArmor = maxArmorStatistic;
            healthService.MaxHealth = maxHealthStatistic;
            healthService.AddHealth(health);
            healthService.AddArmor(armor);
            //Act
            healthService.TakeDamage(damage);
            var isDead = healthService.IsDead();

            //Assert
            Assert.IsTrue(isDead);
        }
    }
}
