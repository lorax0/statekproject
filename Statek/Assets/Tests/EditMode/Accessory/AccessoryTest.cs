﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using UnityEngine.TestTools;
using Statek.Player.Accessory;
using UnityEngine;
using System.Linq;

namespace Tests.EditMode.Accessory
{
    public class AccessoryTest
    {
        [Test]
        public void CanAddAccessoryToEmptySlotWithCorrectType()
        {
            //Arrange
            var slot = new AccessorySlot();
            var accessory = Substitute.For<IAccessory>();
            slot.SlotType = Type.Offensive;
            accessory.AccessoryType.Returns(new List<Type> { Type.Offensive });

            //Act
            var canAdd = slot.IsCorrectType(accessory.AccessoryType);

            //Assert
            Assert.IsTrue(canAdd);
        }

        [Test]
        public void CannotAddAccessoryToEmptySlotWithInCorrectType()
        {
            //Arrange
            var slot = new AccessorySlot();
            var accessory = Substitute.For<IAccessory>();
            slot.SlotType = Type.Defensive;
            accessory.AccessoryType.Returns(new List<Type> { Type.Offensive });

            //Act
            var canAdd = slot.IsCorrectType(accessory.AccessoryType);

            //Assert
            Assert.IsFalse(canAdd);
        }

        [Test]
        public void CanAddAccessoryToEmptySlotWithoutType()
        {
            //Arrange
            var slot = new AccessorySlot();
            var accessory = Substitute.For<IAccessory>();
            slot.SlotType = Type.Default;
            accessory.AccessoryType.Returns(new List<Type> { Type.Default, Type.Offensive });

            //Act
            var canAdd = slot.IsCorrectType(accessory.AccessoryType);

            //Assert
            Assert.IsTrue(canAdd);
        }
        
    }
}