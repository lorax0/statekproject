﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Statek.Resources;

namespace Tests.EditMode.Resources
{
    public class ResourceTest
    {
        [Test]
        public void IncreaseResourceAmountTest()
        {
            //Arrange
            IResource resource = ScriptableObject.CreateInstance<Resource>();
            var expectedAmount = 3;

            //Act
            resource.AddResource(expectedAmount);
            var amount = resource.Amount;

            //Assert
            Assert.AreEqual(expectedAmount, amount);
        }

        [Test]
        public void DecreaseResourceAmountTest()
        {
            //Arrange
            IResource resource = ScriptableObject.CreateInstance<Resource>();
            var expectedAmount = 1;

            //Act
            resource.AddResource(3);
            resource.RemoveResource(2);
            var amount = resource.Amount;

            //Assert
            Assert.AreEqual(expectedAmount, amount);
        }

        [Test]
        public void CantRemoveResourceAmountLessThanZeroTest()
        {
            //Arrange
            IResource resource = ScriptableObject.CreateInstance<Resource>();

            //Act
            resource.AddResource(1);
            var canRemove = resource.CanRemoveResource(2);

            //Assert
            Assert.IsFalse(canRemove);
        }

        public void CantAddResourceAmountMoreThanMaxAmountTest()
        {
            //Arrange
            IResource resource = ScriptableObject.CreateInstance<Resource>();
            var maxAmount = 10;
            resource.MaxAmount = maxAmount;

            //Act
            resource.AddResource(10);
            var canAdd = resource.CanAddResource(2);

            //Assert
            Assert.IsFalse(canAdd);
        }
    }
}
