﻿using System;
using UnityEngine;
using Statek.Research;

namespace Statek.Movement
{
    [Serializable]
    public class Mover : IMover
    {
        public Rigidbody2D Rigidbody { get => this.rigidbody; set => this.rigidbody = value; }
        public Transform Transform => this.rigidbody.transform;
        public Transform PlayerObject => this.playerObject;
        public IResearchableStatistic MovementSpeed => this.movementSpeed;

        [SerializeField] protected ResearchableStatistic movementSpeed;
        [SerializeField] protected Transform playerObject;

        protected Rigidbody2D rigidbody;
        protected const float movementMultiplier = 0.2f;

        public void Move(Vector2 direction)
        {
            bool destinyReached = Mathf.Abs(direction.x) < 0.1f && Mathf.Abs(direction.y) < 0.1f;

            if (!destinyReached)
            {
                this.FollowDirection(direction);
            }
            else
            {
                this.Break();
            }
        }

        public bool IsMoving() => this.rigidbody.velocity != Vector2.zero;

        private void FollowDirection(Vector2 direction)
        {
            this.rigidbody.velocity = direction.normalized * this.movementSpeed.Statistic * movementMultiplier;
            this.playerObject.up = direction;
        }

        public void Break()
        {
            this.rigidbody.velocity = Vector2.zero;
        }
    }
}