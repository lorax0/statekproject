﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Movement
{
    public interface IMovementType
    {
        Transform Transform { get; set; }
        Vector2 GetDirection();
    }
}