﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Movement
{
    [CreateAssetMenu(fileName = "MouseFollower", menuName = "Statek/Movement/MouseFollower")]
    public class MouseFollower : MovementType
    {
        protected Vector3 mousePosition;
        public override Vector2 GetDirection()
        {
            if (Input.GetMouseButton(0))
            {
                this.mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            Vector2 direction = this.mousePosition - this.transform.position;
            return direction;
        }
    }
}