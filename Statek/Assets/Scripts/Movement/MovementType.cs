﻿using System;
using UnityEngine;

namespace Statek.Movement
{
    public class MovementType : ScriptableObject, IMovementType
    {
        public Transform Transform { get => this.transform; set => this.transform = value; }

        protected Transform transform;

        public virtual Vector2 GetDirection()
        {
            return Vector2.zero;
        }
    }
}
