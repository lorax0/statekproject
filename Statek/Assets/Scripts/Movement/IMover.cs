﻿using UnityEngine;
using Statek.Research;

namespace Statek.Movement
{
    public interface IMover
    {
        Rigidbody2D Rigidbody { get; set; }

        Transform Transform { get; }

        Transform PlayerObject { get; }

        IResearchableStatistic MovementSpeed { get; }

        void Move(Vector2 direction);

        void Break();
    }
}
