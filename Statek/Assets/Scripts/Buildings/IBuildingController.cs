﻿namespace Statek.Buildings
{
    public interface IBuildingController
    {
        IBuilding Building { get; }
    }
}