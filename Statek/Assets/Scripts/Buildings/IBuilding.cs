﻿using System;
using System.Collections.Generic;
using Statek.Resources;

namespace Statek.Buildings
{
    public interface IBuilding
    {
        string Name { get; set; }
        string Description { get; }
        int CurrentLevel { get; set; }
        int MaxLevel { get; set; }
        float BuildTime { get; set; }
        IEnumerable<IResourceUnit> Resources { get; }
        event EventHandler<EventArgs> OnValueChanged;
    }
}