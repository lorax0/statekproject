﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Buildings
{
    public class BuildingController : MonoBehaviour, IBuildingController
    {
        public IBuilding Building => this.building;

        [SerializeField] private Building building = null;
    }
}