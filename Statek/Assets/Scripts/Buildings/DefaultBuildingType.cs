﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Buildings
{
    public class DefaultBuildingType : Building, IBuildingType, IBuildable, IUpgradable
    {
        public bool Buildable { get => this.buildable; set => this.buildable = value; }
        
        [SerializeField] protected bool buildable;

        public virtual void Build()
        {
            if(this.buildable)
            {
                this.buildable = false;
                this.CurrentLevel = 1;
                //do something
            }
        }

        public bool CanUpgrade(int levelCount = 1)
        {
            if(this.currentLevel == 0)
            {
                return false;
            }
            return (this.currentLevel + levelCount) <= this.maxLevel;
        }

        public void Upgrade()
        {
            if (this.CanUpgrade())
            {
                this.CurrentLevel++;
            }
        }

        public void Upgrade(int levelCount)
        {
            if (this.CanUpgrade(levelCount))
            {
                this.CurrentLevel += levelCount;
            }
        }
    }
}