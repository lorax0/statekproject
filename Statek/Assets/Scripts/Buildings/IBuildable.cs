﻿using UnityEngine;

namespace Statek.Buildings
{
    public interface IBuildable
    {
        bool Buildable { get; set; }
        void Build();
    }
}