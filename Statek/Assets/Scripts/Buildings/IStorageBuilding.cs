﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Buildings
{
    public interface IStorageBuilding
    {
        int Capacity { get;}
        int CapacityPerLevel { get; }
    }
}