﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Statek.Buildings
{
    public class BuildCostPrefabManager : MonoBehaviour, IBuildCostPrefabManager
    {
        public Text CostText => this.costText;
        public Image CostImage => this.costImage;
        
        [SerializeField] protected Text costText;
        [SerializeField] protected Image costImage;

        public void SetCostText(string name) => this.costText.text = name;

        public void SetCostImage(Sprite sprite) => this.costImage.sprite = sprite;
    }
}