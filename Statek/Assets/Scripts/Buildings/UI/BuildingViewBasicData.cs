﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Statek.Resources;
using System;

namespace Statek.Buildings
{
    [Serializable]
    public class BuildingViewBasicData : IBuildingViewBasicData
    {
        public Text Name => this.nameText;
        public Text Description => this.descriptionText;
        public Text Level => this.levelText;
        public Text BuildTime => this.buildTimeText;
        public GameObject BuildCostHandler => this.buildCostHandler;
        public GameObject BuildCostPrefab => this.buildCostPrefab;

        [SerializeField] protected Text nameText;
        [SerializeField] protected Text descriptionText;
        [SerializeField] protected Text levelText;
        [SerializeField] protected Text buildTimeText;
        [SerializeField] protected GameObject buildCostHandler;
        [SerializeField] protected GameObject buildCostPrefab;


        public void DisplayName(DefaultBuildingType building)
        {
            this.nameText.text = building.Name;
        }

        public void DisplayDescription(DefaultBuildingType building)
        {
            this.descriptionText.text = building.Description;
        }

        public void DisplayLevel(DefaultBuildingType building)
        {
            this.levelText.text = building.CurrentLevel.ToString() + " / " + building.MaxLevel.ToString();
        }

        public void DisplayBuildTime(DefaultBuildingType building)
        {
            this.buildTimeText.text = building.BuildTime.ToString();
        }

        public void DisplayBuildCost(DefaultBuildingType building)
        {
            IEnumerable<IResourceUnit> resourceList = building.Resources;

            foreach (var element in resourceList)
            {
                GameObject resource = MonoBehaviour.Instantiate(this.buildCostPrefab, this.buildCostHandler.transform);

                var prefabManager = resource.GetComponent<BuildCostPrefabManager>();

                prefabManager.SetCostText(element.Resource.Name + ": " + element.RequiredAmount.ToString());
                prefabManager.SetCostImage(element.Resource.Image);
            }
        }
    }
}