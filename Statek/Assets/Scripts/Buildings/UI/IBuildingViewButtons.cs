﻿using UnityEngine;
using UnityEngine.UI;

namespace Statek.Buildings
{
    public interface IBuildingViewButtons
    {
        Button BuildButton { get; }
        Button UpgradeButton { get; }
        void SetupBuildButton(DefaultBuildingType building);
        void SetupUpgradeButton(DefaultBuildingType building);
    }
}