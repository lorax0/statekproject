﻿using UnityEngine;
using UnityEngine.UI;

namespace Statek.Buildings
{
    public interface IBuildCostPrefabManager
    {
        Text CostText { get; }
        Image CostImage { get; }
        void SetCostText(string name);
        void SetCostImage(Sprite sprite);
    }
}