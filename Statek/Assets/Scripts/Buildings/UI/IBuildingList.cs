﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Buildings
{
    public interface IBuildingList
    {
        GameObject Handler { get; }
        IBuildingView Prefab { get; }
        IEnumerable<IBuilding> Buildings { get; }
        void Create();
    }
}