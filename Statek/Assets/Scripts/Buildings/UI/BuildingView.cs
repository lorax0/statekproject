﻿using System;
using UnityEngine;

namespace Statek.Buildings
{
    public class BuildingView : MonoBehaviour, IBuildingView
    {
        public IBuildingType Building { get => this.building; set => this.building = (DefaultBuildingType)value; }
        public IBuildingViewButtons BuildingViewButtons => this.buttons;
        public IBuildingViewBasicData BuildingViewBasicData => this.basicData;

        [SerializeField] protected DefaultBuildingType building;
        [SerializeField] protected BuildingViewButtons buttons;
        [SerializeField] protected BuildingViewBasicData basicData;

        public void Initialize()
        {
            UpdateBasicData(this, new EventArgs { });
            this.building.OnValueChanged += UpdateBasicData;
            this.buttons.SetupBuildButton(this.building);
            this.buttons.SetupUpgradeButton(this.building);
        }

        public void UpdateBasicData(object sender, EventArgs e)
        {
            this.basicData.DisplayName(this.building);
            this.basicData.DisplayDescription(this.building);
            this.basicData.DisplayLevel(this.building);
            this.basicData.DisplayBuildTime(this.building);
            this.basicData.DisplayBuildCost(this.building);

            if (!this.building.Buildable)
            {
                buttons.BuildButton.gameObject.SetActive(false);
            }
        }
    }
}