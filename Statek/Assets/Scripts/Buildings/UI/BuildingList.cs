﻿using System.Collections.Generic;
using UnityEngine;

namespace Statek.Buildings
{
    public class BuildingList : MonoBehaviour, IBuildingList
    {
        public GameObject Handler => this.handler;
        public IBuildingView Prefab => this.prefab;
        IEnumerable<IBuilding> IBuildingList.Buildings => this.buildings;

        [SerializeField] protected GameObject handler;
        [SerializeField] protected BuildingView prefab;
        [SerializeField] protected List<Building> buildings;

        private void Awake()
        {
            if (this.handler == null)
            {
                Debug.LogWarning("A warning assigned to building list handler!", this);
            }

            if (this.prefab == null)
            {
                Debug.LogWarning("A warning assigned to building list prefab!", this);
            }
        }

        public void Create()
        {
            foreach (var element in this.buildings)
            {
                BuildingView listElement = Instantiate(this.prefab, this.handler.transform.position,
                                                     this.handler.transform.rotation, this.handler.transform);
                
                listElement.Building = (IBuildingType)element;
                listElement.Initialize();
            }
        }
    }
}