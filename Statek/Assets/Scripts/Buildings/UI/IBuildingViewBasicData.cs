﻿using UnityEngine;
using UnityEngine.UI;

namespace Statek.Buildings
{
    public interface IBuildingViewBasicData
    {
        Text Name { get; }
        Text Description { get; }
        Text Level { get; }
        Text BuildTime { get; }
        GameObject BuildCostPrefab { get; }
        GameObject BuildCostHandler { get; }
        void DisplayName(DefaultBuildingType building);
        void DisplayDescription(DefaultBuildingType building);
        void DisplayLevel(DefaultBuildingType building);
        void DisplayBuildTime(DefaultBuildingType building);
        void DisplayBuildCost(DefaultBuildingType building);
    }
}