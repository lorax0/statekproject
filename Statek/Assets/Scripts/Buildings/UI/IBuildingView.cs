﻿using System;

namespace Statek.Buildings
{
    public interface IBuildingView
    {
        IBuildingType Building { get; set; }
        IBuildingViewButtons BuildingViewButtons { get; }
        IBuildingViewBasicData BuildingViewBasicData { get; }
        void Initialize();
        void UpdateBasicData(object sender, EventArgs e);
    }
}