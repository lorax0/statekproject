﻿public interface IUpgradable
{
    bool CanUpgrade(int levelCount);
    void Upgrade();
    void Upgrade(int levelCount);
}