﻿using System;
using System.Collections.Generic;
using Statek.Resources;
using UnityEngine;

namespace Statek.Research
{
    public interface IResearchManager
    {
        Sprite ObtainIcon { get; }
        Sprite BlockIcon { get; }

        bool HaveEnoughMaterials(IEnumerable<IResourceUnit> resources);
        void UpdateResearchImages(object sender, EventArgs args);
    }
}