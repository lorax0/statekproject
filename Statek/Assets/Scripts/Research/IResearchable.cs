﻿namespace Statek.Research
{
    public interface IResearchable
    {
        void Research();
    }
}