﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Statek.Research
{
    public interface IResearchImage
    {
        Image StateImage { get; }

        Sprite BlockIcon { set; }

        Sprite ObtainIcon { set; }

        void Lock();

        void Unlock();

        void Obtain(object sender, EventArgs args);
    }
}