﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Research
{
    [CreateAssetMenu(fileName = "NewStatistic", menuName = "Statek/Statistic")]
    public class ResearchableStatistic : ScriptableObject, IResearchableStatistic
    {
        public float Statistic { get => this.statistic; set => this.statistic = value; }
        public float StatisticOffSet { get => this.statisticOffSet; set => this.statisticOffSet = value; }

        public event EventHandler<EventArgs> OnStatisticValueChange;

        [SerializeField] protected float statistic;
        [SerializeField] protected float statisticOffSet;

        public void Research()
        {
            this.statistic += this.statisticOffSet;
            this.OnStatisticValueChange?.Invoke(this, EventArgs.Empty);
        }
    }
}
