﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Statek.Research
{
    [Serializable]
    public class ResearchImage : IResearchImage
    {
        public Image StateImage => this.stateImage;
        public Sprite BlockIcon { set => this.blockIcon = value; }
        public Sprite ObtainIcon { set => this.obtainIcon = value; }

        [SerializeField] protected Image stateImage;

        protected Sprite blockIcon;
        protected Sprite obtainIcon;

        public void Lock() => this.stateImage.sprite = this.blockIcon;

        public void Unlock() => this.stateImage.sprite = null;

        public void Obtain(object sender, EventArgs args) => this.stateImage.sprite = this.obtainIcon;
    }
}