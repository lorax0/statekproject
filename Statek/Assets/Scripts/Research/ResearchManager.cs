﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Statek.Resources;

namespace Statek.Research
{
    public class ResearchManager : MonoBehaviour, IResearchManager
    {
        public IEnumerable<IResearchController> Researches => this.researches;
        public Sprite ObtainIcon => this.obtainIcon;
        public Sprite BlockIcon => this.blockIcon;

        [SerializeField] protected Sprite obtainIcon;
        [SerializeField] protected Sprite blockIcon;

        protected IEnumerable<IResearchController> researches;

        private void Start()
        {
            this.SetResearch();
        }

        private void SetResearch()
        {
            this.researches = this.GetComponentsInChildren<IResearchController>();
            foreach(var research in this.researches)
            {
                research.Research.ResearchManager = this;
                research.Research.SetStartValue();
            }
        }

        public bool HaveEnoughMaterials(IEnumerable<IResourceUnit> resources)
        {
            return true;
        }

        public void UpdateResearchImages(object sender, EventArgs args)
        {
            foreach(var research in this.researches)
            {
                if (research.Research.ParentsIsUnlocked() && !research.Research.IsObtained)
                {
                    research.Research.Unlock();
                }
            }
        }
    }
}