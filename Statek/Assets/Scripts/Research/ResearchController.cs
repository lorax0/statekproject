﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Statek.Research
{
    public class ResearchController : MonoBehaviour, IResearchController, IPointerDownHandler
    {
        public IResearch Research => this.research;

        [SerializeField] protected Research research;

        public void OnPointerDown(PointerEventData eventData) => this.research.OnClick();
    }
}