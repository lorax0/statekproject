﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Statek.Resources;

namespace Statek.Research
{
    [Serializable]
    public class Research : IResearch
    {
        public bool IsObtained => this.isObtained;
        public IEnumerable<IResearchController> ParentResearch => this.parentResearch;
        public IResearchManager ResearchManager { set { this.researchManager = value; } }
        public IEnumerable<IResourceUnit> Resources => this.resources;
        public IResearchImage ResearchImage => this.researchImage;
        public IResearchable Result => (IResearchable) this.result;

        public event EventHandler<EventArgs> OnObtain;

        [SerializeField] protected List<ResearchController> parentResearch = new List<ResearchController>();
        [SerializeField] protected List<ResourceUnit> resources = new List<ResourceUnit>();
        [SerializeField] protected ResearchImage researchImage;
        [SerializeField] protected ScriptableObject result;

        protected IResearchManager researchManager;
        protected bool isObtained;

        public void SetStartValue()
        {
            this.researchImage.ObtainIcon = this.researchManager.ObtainIcon;
            this.researchImage.BlockIcon = this.researchManager.BlockIcon;
            this.CheckCorrectResult();
            this.SubscribeEvents();
            this.SetCorrectIcon();
        }

        public void SubscribeEvents()
        {
            this.OnObtain += this.researchImage.Obtain;
            this.OnObtain += this.ConductResearch;
            this.OnObtain += this.researchManager.UpdateResearchImages;
        }

        public void OnClick()
        {
            if (this.CanBeObtained())
            {
                this.Obtain();
            }
        }

        public bool CanBeObtained()
        {
            if (this.isObtained)
            {
                return false;
            }
            if (!this.ParentsIsUnlocked())
            {
                return false;
            }
            if (!this.researchManager.HaveEnoughMaterials(this.resources))
            {
                return false;
            }
            return true;
        }
        
        public void Obtain()
        {
            this.isObtained = true;
            this.OnObtain?.Invoke(this, EventArgs.Empty);
        }

        public void ConductResearch(object sender, EventArgs args)
        {
            this.Result.Research();
        }

        public void Lock() => this.researchImage.Lock();

        public void Unlock() => this.researchImage.Unlock();

        public bool ParentsIsUnlocked()
        {
            foreach (var parent in this.parentResearch)
            {
                if (!parent.Research.IsObtained)
                {
                    return false;
                }
            }
            return true;
        }
        
        private void CheckCorrectResult()
        {
            try
            {
                var check = this.Result;
            }
            catch (InvalidCastException)
            {
                Debug.LogError("This scriptable object can not be result of research");
            }
        }

        private void SetCorrectIcon()
        {
            if (this.isObtained)
            {
                this.Obtain();
                return;
            }
            if (!this.ParentsIsUnlocked())
            {
                this.Lock();
                return;
            }
        }

    }
}