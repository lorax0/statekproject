﻿using System;
using System.Collections.Generic;
using Statek.Resources;

namespace Statek.Research
{
    public interface IResearch
    {
        bool IsObtained { get; }

        IEnumerable<IResearchController> ParentResearch { get; }

        IResearchManager ResearchManager { set; }

        IEnumerable<IResourceUnit> Resources { get; }

        IResearchable Result { get; }
        
        event EventHandler<EventArgs> OnObtain;

        void OnClick();

        bool CanBeObtained();

        void Obtain();

        void Lock();

        void Unlock();

        bool ParentsIsUnlocked();

        void ConductResearch(object sender, EventArgs args);

        void SetStartValue();

        void SubscribeEvents();
    }
}