﻿namespace Statek.Research
{
    public interface IResearchController
    {
        IResearch Research { get; }
    }
}