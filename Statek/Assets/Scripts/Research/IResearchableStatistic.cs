﻿using System;

namespace Statek.Research
{
    public interface IResearchableStatistic : IResearchable
    {
        float Statistic { get; set; }

        float StatisticOffSet { get; set; }

        event EventHandler<EventArgs> OnStatisticValueChange;
    }
}