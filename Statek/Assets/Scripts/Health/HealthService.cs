﻿using UnityEngine;
using System;
using Statek.Research;

namespace Statek.Health
{
    [Serializable]
    public class HealthService : IHealthService
    {
        public IResearchableStatistic MaxHealth { get => this.maxHealth; set => this.maxHealth = (ResearchableStatistic) value; }
        public float Health => this.health;
        public IResearchableStatistic MaxArmor { get => this.maxArmor; set => this.maxArmor = (ResearchableStatistic)value; }
        public float Armor => this.armor;

        [SerializeField] protected ResearchableStatistic maxHealth;
        [SerializeField] protected float health;
        [SerializeField] protected ResearchableStatistic maxArmor;
        [SerializeField] protected float armor;

        public void TakeDamage(float damage)
        {
            var damageTaken = this.armor - damage;
            var damageAfterArmor = damageTaken < 0f ? Mathf.Abs(damageTaken) : 0;
            this.armor = Mathf.Clamp(damageTaken, 0, this.maxArmor.Statistic);
            this.health = Mathf.Clamp(this.health - damageAfterArmor, 0, this.maxHealth.Statistic);
        }

        public void AddHealth(float health)
        {
            this.health += health;
            this.health = Mathf.Clamp(this.health, 0, this.maxHealth.Statistic);
        }

        public void AddArmor(float armor)
        {
            this.armor += armor;
            this.armor = Mathf.Clamp(this.armor, 0, this.maxArmor.Statistic);
        }

        public bool IsDead() => this.health == 0f;

    }
}