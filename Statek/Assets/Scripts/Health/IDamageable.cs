﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Health
{
    public interface IDamageable
    {
        Transform Transform { get; }
    }
}
