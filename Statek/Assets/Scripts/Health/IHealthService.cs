﻿using Statek.Research;

namespace Statek.Health
{
    public interface IHealthService
    {
        IResearchableStatistic MaxHealth { get; set; }
        float Health { get; }
        IResearchableStatistic MaxArmor { get; set; }
        float Armor { get; }
        void TakeDamage(float damage);
        void AddHealth(float health);
        void AddArmor(float armor);
        bool IsDead();
    }
}
