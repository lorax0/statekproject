﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Statek.Player.Accessory
{
    public class AccessoryMenuController : MonoBehaviour, IAccessoryMenuController, IPointerDownHandler
    {

        public IAccessoryMenu AccessoryMenu => this.accessoryMenu;
        public GameObject AccessoryUI => this.accessoryUI;

        public event EventHandler<PointerEventArgs> OnBackgroundClick;

        [SerializeField] protected AccessoryMenu accessoryMenu;
        [SerializeField] protected GameObject accessoryUI;
        [SerializeField] protected Accessory accessory;
        [SerializeField] protected Accessory accessory2;

        private void Start()
        {
            this.accessoryMenu.SubscribeEvents();
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory2);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory2);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory2);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory);
            this.accessoryMenu.AddAccessory(accessory2);
            this.OnBackgroundClick += this.accessoryMenu.OnBackgroundClick;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                this.accessoryUI.SetActive(!this.accessoryUI.activeSelf);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            this.OnBackgroundClick?.Invoke(this, new PointerEventArgs { EventData = eventData });
        }
    }
}