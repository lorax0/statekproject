﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Statek.Player.Accessory
{
    public class AccessoryViewController : MonoBehaviour, IAccessoryViewController, IPointerDownHandler
    {
        public IAccessory Accessory { get => this.accessory; set => this.accessory = value; }
        public Image Image => this.image;

        public event EventHandler<PointerEventArgs> OnRightMouseClick;

        [SerializeField] protected Image image;

        protected IAccessory accessory;

        public void SetAccessoryImage()
        {
            this.image.sprite = this.accessory.Sprite;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            this.OnRightMouseClick?.Invoke(this, new PointerEventArgs { EventData = eventData});
        }
    }
}
