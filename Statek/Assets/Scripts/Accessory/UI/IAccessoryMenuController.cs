﻿using System;
using UnityEngine;

namespace Statek.Player.Accessory
{
    public interface IAccessoryMenuController
    {
        event EventHandler<PointerEventArgs> OnBackgroundClick;
        IAccessoryMenu AccessoryMenu { get; }
        GameObject AccessoryUI { get; }
    }
}