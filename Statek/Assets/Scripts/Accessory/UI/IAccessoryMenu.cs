﻿using UnityEngine;
using System.Collections.Generic;

namespace Statek.Player.Accessory
{
    public interface IAccessoryMenu
    {
        GameObject ContentMenu { get; }
        AccessoryViewController AccessoryViewPrefab { get; }
        IEnumerable<IAccessoryViewController> AccessoryViews { get; }
        IAccessorySlot SelectedSlot { get; }
        void AddAccessory(IAccessory accessory);
        void OnAccessoryClick(object sender, PointerEventArgs args);
        void OnSlotClick(object sender, PointerEventArgs args);
        void OnBackgroundClick(object sender, PointerEventArgs args);
        void SortAccessoryViews(IAccessorySlot slot);
        void ShowMenu();
        void HideMenu();
    }
}