﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Linq;

namespace Statek.Player.Accessory
{
    [Serializable]
    public class AccessoryMenu : IAccessoryMenu
    {
        public GameObject ContentMenu => this.contentMenu;
        public AccessoryViewController AccessoryViewPrefab => this.accessoryViewPrefab;
        public IEnumerable<IAccessoryViewController> AccessoryViews => this.accessoryViews;
        public IAccessorySlot SelectedSlot => this.selectedSlot;

        [SerializeField] protected GameObject contentMenu;
        [SerializeField] protected AccessoryViewController accessoryViewPrefab;
        [SerializeField] protected List<AccessorySlotController> slots;
        protected List<AccessoryViewController> accessoryViews = new List<AccessoryViewController>();
        protected IAccessorySlot selectedSlot;
        
        public void SubscribeEvents()
        {
            foreach(var slot in this.slots)
            {
                slot.OnSlotClick += this.OnSlotClick;
            }
        }

        public void AddAccessory(IAccessory accessory)
        {
            IAccessoryViewController accessoryView = this.CreateAccessoryView();
            accessoryView.Accessory = accessory;
            accessoryView.OnRightMouseClick += OnAccessoryClick;
            accessoryView.SetAccessoryImage();
            this.accessoryViews.Add((AccessoryViewController)accessoryView);
        }

        private IAccessoryViewController CreateAccessoryView()
        {
            AccessoryViewController accessoryView = MonoBehaviour.Instantiate(this.accessoryViewPrefab, this.contentMenu.transform);
            return accessoryView;
        }

        public void OnAccessoryClick(object sender, PointerEventArgs args)
        {
            if (args.EventData.button != PointerEventData.InputButton.Left) return;
            if (this.selectedSlot == null) return;
            IAccessoryViewController accessoryView = (IAccessoryViewController)sender;
            if (!this.selectedSlot.IsEmpty())
            {
                this.SetViewActivity(this.selectedSlot.CurrentAccessoryView, true);
                this.selectedSlot.Remove();
            }
            this.selectedSlot.Add(accessoryView);
            this.SetViewActivity(accessoryView, false);
        }

        private void SetViewActivity(IAccessoryViewController accessoryView, bool value)
        {
            ((AccessoryViewController)accessoryView).gameObject.SetActive(value);
        }

        public void OnSlotClick(object sender, PointerEventArgs args)
        {
            if (args.EventData.button != PointerEventData.InputButton.Left) return;
            IAccessorySlot slot = ((IAccessorySlotController)sender).Slot;
            if (!slot.IsEmpty() && this.selectedSlot == slot) slot.Remove();
            this.selectedSlot = slot;
            this.SortAccessoryViews(slot);
        }

        public void OnBackgroundClick(object sender, PointerEventArgs args)
        {
            this.selectedSlot = null;
            this.ResetAccessoriesVisibility();
        }

        public void SortAccessoryViews(IAccessorySlot slot)
        {
            foreach(var view in this.accessoryViews)
            {
                IEnumerable<Type> accessoryTypes = view.Accessory.AccessoryType;
                bool isCorectType = slot.IsCorrectType(accessoryTypes);
                bool isSlotViewUsed = this.IsViewUsed(view);
                if (!isCorectType || isSlotViewUsed)
                {
                    this.SetViewActivity(view, false);
                    continue;
                }
                this.SetViewActivity(view, true);
            }
        }

        private bool IsViewUsed(IAccessoryViewController view)
        {
            foreach(var slot in this.slots)
            {
                if (slot.Slot.CurrentAccessoryView == view)
                {
                    return true;
                }
            }
            return false;
        }

        private void ResetAccessoriesVisibility()
        {
            foreach (var view in this.accessoryViews)
            {
                bool isSlotViewUsed = this.IsViewUsed(view);
                if (isSlotViewUsed)
                {
                    this.SetViewActivity(view, false);
                    continue;
                }
                this.SetViewActivity(view, true);
            }
        }

        public void ShowMenu() => this.contentMenu.SetActive(true);

        public void HideMenu() => this.contentMenu.SetActive(false);
    }
}