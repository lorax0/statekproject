﻿using System;

namespace Statek.Player.Accessory
{
    public interface IAccessoryViewController
    {
        event EventHandler<PointerEventArgs> OnRightMouseClick;
        IAccessory Accessory { get; set; }
        void SetAccessoryImage();
    }
}