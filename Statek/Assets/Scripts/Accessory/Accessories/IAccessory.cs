﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Player.Accessory
{
    public interface IAccessory
    {
        Sprite Sprite { get; }
        IEnumerable<Type> AccessoryType { get; }
    }
}
