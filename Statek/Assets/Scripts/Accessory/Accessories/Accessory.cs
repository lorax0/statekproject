﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Player.Accessory
{
    [CreateAssetMenu(fileName = "Accessory", menuName = "Statek/Accessory")]
    public class Accessory : ScriptableObject, IAccessory
    {
        public IEnumerable<Type> AccessoryType => this.accessoryTypes;
        public Sprite Sprite => this.sprite;
        [SerializeField] protected List<Type> accessoryTypes;
        [SerializeField] protected Sprite sprite;
    }
}
