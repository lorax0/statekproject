﻿using System;

namespace Statek.Player.Accessory
{
    public interface IAccessorySlotController
    {
        event EventHandler<PointerEventArgs> OnSlotClick;
        IAccessorySlot Slot { get; }
    }
}