﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Statek.Player.Accessory
{
    public interface IAccessorySlot
    {
        IAccessoryViewController CurrentAccessoryView { get; set; }
        Type SlotType { get; set; }
        Image Image { get; }
        IAccessory Accessory { get; }
        void Add(IAccessoryViewController accessory);
        void Remove();
        IAccessory GetAccessory();
        bool IsCorrectType(IEnumerable<Type> accessoryType);
        bool IsEmpty();
    }
}
