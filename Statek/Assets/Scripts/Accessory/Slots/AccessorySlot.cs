﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

namespace Statek.Player.Accessory
{
    public enum Type {  Default, Offensive, Laser, Rocket, Aura, Defensive,
        SelfDefensive, SupportDefensive, Repair, SelfRepair, SupportRepair, Movement, Storage}

    [Serializable]
    public class AccessorySlot : IAccessorySlot
    {
        public IAccessory Accessory => this.currentAccessoryView.Accessory;
        public Type SlotType { get => this.slotType; set => this.slotType = value; }
        public IAccessoryViewController CurrentAccessoryView { get => this.currentAccessoryView; set => this.currentAccessoryView = value; } 
        public Image Image => this.image;
        
        [SerializeField] protected Type slotType;
        [SerializeField] protected Image image;

        protected IAccessoryViewController currentAccessoryView;

        public void Add(IAccessoryViewController viewController)
        {
            this.currentAccessoryView = viewController;
            this.image.sprite = viewController.Accessory.Sprite;
        }

        public void Remove()
        {
            this.currentAccessoryView = null;
            this.image.sprite = null;
        }

        public IAccessory GetAccessory() => this.currentAccessoryView?.Accessory;

        public bool IsCorrectType(IEnumerable<Type> accessoryType)
        {
            var correctType = accessoryType.Where(t => t == this.slotType);
            if(correctType.Count() != 0)
            {
                return true;
            }
            return false;

        }

        public bool IsEmpty() => this.GetAccessory() == null;

    }
}
