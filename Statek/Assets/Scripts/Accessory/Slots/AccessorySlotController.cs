﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;

namespace Statek.Player.Accessory
{
    public class AccessorySlotController : MonoBehaviour, IAccessorySlotController, IPointerDownHandler
    {
        public IAccessorySlot Slot => this.slot;

        public event EventHandler<PointerEventArgs> OnSlotClick;

        [SerializeField] protected AccessorySlot slot;

        public void OnPointerDown(PointerEventData eventData)
        {
            this.OnSlotClick?.Invoke(this, new PointerEventArgs { EventData = eventData });
        }
    }
}