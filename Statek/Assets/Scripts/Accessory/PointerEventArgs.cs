﻿using System;
using UnityEngine.EventSystems;

namespace Statek.Player.Accessory
{
    public class PointerEventArgs : EventArgs
    {
        public PointerEventData EventData;
    }
}
