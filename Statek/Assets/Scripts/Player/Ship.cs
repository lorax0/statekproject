﻿using System;
using UnityEngine;
using Statek.Movement;
using Statek.Health;
using Statek.Particles;

namespace Statek.Player
{
    [Serializable]
    public class Ship : IShip
    {
        public IMover Mover => this.mover;
        public IMovementType MovementType => this.movementType;
        public Transform Transform => this.transform;
        public IHealthService HealthService => this.healthService;
        public IParticleSystem MovementParticleSystem => this.movementParticleSystem;
        [SerializeField] protected Mover mover;
        [SerializeField] protected HealthService healthService;
        [SerializeField] protected MovementType movementType;
        [SerializeField] protected Transform transform;
        [SerializeField] protected MovementParticleSystem movementParticleSystem;

        public void Initialize()
        {
            this.mover.Rigidbody = Transform.GetComponent<Rigidbody2D>();
            this.movementType.Transform = Transform;
        }

        public void Move()
        {
            var direction = this.movementType.GetDirection();
            this.mover.Move(direction);
            this.movementParticleSystem.SetParticlesActivity(this.mover.IsMoving());
        }
    }
}
