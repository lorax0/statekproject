﻿using System.Collections.Generic;
using UnityEngine;
using Statek.Movement;
using Statek.Health;
using Statek.Particles;

namespace Statek.Player
{
    public interface IShip
    {
        IMover Mover { get; }

        Transform Transform { get; }

        IMovementType MovementType { get; }

        IHealthService HealthService { get; }

        IParticleSystem MovementParticleSystem { get; }

        void Move();
    }
}