﻿using Statek.Movement;

namespace Statek.Player
{
    public interface IShipController
    {
        IShip Ship { get; }
    }
}