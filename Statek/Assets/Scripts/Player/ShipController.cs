﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Statek.Health;

namespace Statek.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class ShipController : MonoBehaviour, IShipController, IDamageable
    {
        public IShip Ship => this.ship;

        public Transform Transform => this.transform;

        [SerializeField] Ship ship = null;

        private void Awake()
        {
            this.ship.Initialize();
            DontDestroyOnLoad(this.gameObject);
        }

        private void Update()
        {
            this.ship.Move();
        }
    }
}