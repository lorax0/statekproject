﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Statek.UI.Menu
{
    public class MenuManager : MonoBehaviour
    {
        public IShipSelectionManager ShipSelectionManager => this.shipSelectionManager;

        [SerializeField] protected ShipSelectionManager shipSelectionManager;
        [SerializeField] protected Transform settings;
        [SerializeField] protected Transform menuButtons;

        private void Awake()
        {
            this.shipSelectionManager.SubscribeEvents();
        }

        public void EnableSelectionMenu()
        {
            this.shipSelectionManager.EnableSelectionMenu();
            this.DisableMenuButtons();
        }

        public void DisableSelectionMenu()
        {
            this.shipSelectionManager.DisableSelectionMenu();
            this.DisableMenuButtons();
        }

        public void StartGame()
        {
            if (!this.shipSelectionManager.IsShipSelected()) return;
            this.shipSelectionManager.CreateShip();
            SceneManager.LoadScene("Main Scene");
        }

        public void OpenSettings() => this.settings.gameObject.SetActive(true);

        public void ExitGame() => Application.Quit();

        private void DisableMenuButtons() => this.menuButtons.gameObject.SetActive(false);
    }
}
