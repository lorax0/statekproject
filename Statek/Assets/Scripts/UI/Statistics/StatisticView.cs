﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Statek.Research;

namespace Statek.UI.Statistics
{
    public class StatisticView : MonoBehaviour, IStatisticView
    {
        public IResearchableStatistic Statistic => this.statistic;
        public Text StatisticText => this.statisticText;
        
        [SerializeField] protected ResearchableStatistic statistic;
        [SerializeField] protected Text statisticText;

        private void OnEnable()
        {
            this.statistic.OnStatisticValueChange += this.UpdateText;
            this.UpdateText(this, EventArgs.Empty);
        }

        private void OnDisable()
        {
            this.statistic.OnStatisticValueChange -= this.UpdateText;
        }

        public void UpdateText(object sender, EventArgs args)
        {
            this.statisticText.text = this.statistic.Statistic.ToString();
        }
    }
}
