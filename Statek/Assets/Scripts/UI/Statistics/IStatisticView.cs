﻿using System;
using UnityEngine.UI;
using Statek.Research;

namespace Statek.UI.Statistics
{
    public interface IStatisticView
    {
        IResearchableStatistic Statistic { get; }

        Text StatisticText { get; }

        void UpdateText(object sender, EventArgs args);
    }
}