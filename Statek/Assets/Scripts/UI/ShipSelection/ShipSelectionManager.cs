﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.UI.Menu
{
    [Serializable]
    public class ShipSelectionManager : IShipSelectionManager
    {
        public GameObject ShipSelectionMenu => this.shipSelectionMenu;
        public IEnumerable<IShipController> ShipControllers => this.shipControllers;
        public IShip SelectedShip => this.selectedShip;

        [SerializeField] protected GameObject shipSelectionMenu = null;
        [SerializeField] protected List<ShipController> shipControllers = new List<ShipController>();

        protected IShip selectedShip;
        
        public void SubscribeEvents()
        {
            foreach(var shipController in this.shipControllers)
            {
                shipController.Ship.OnShipSelect += this.SelectShip;
            }
        }

        public void SelectShip(object sender, EventArgs args)
        {
            IShip ship = (IShip) sender;
            this.selectedShip?.Unhighlight();
            this.selectedShip = ship;
            ship.Highlight();
        }

        public bool IsShipSelected() => this.selectedShip != null;

        public void CreateShip() => MonoBehaviour.Instantiate(this.selectedShip.ShipPrefab);

        public void DisableSelectionMenu() => this.shipSelectionMenu?.SetActive(false);

        public void EnableSelectionMenu() => this.shipSelectionMenu?.SetActive(true);
    }
}