﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Statek.UI.Menu
{
    [Serializable]
    public class Ship : IShip
    {
        public GameObject ShipPrefab { get => this.shipPrefab; set => this.shipPrefab = value; }

        public event EventHandler<EventArgs> OnShipSelect;

        [SerializeField] protected GameObject shipPrefab;
        [SerializeField] protected Image image;

        
        public void OnShipClick()
        {
            this.OnShipSelect?.Invoke(this, EventArgs.Empty);
        }

        public void Highlight()
        {
            this.image.color = Color.yellow;
        }

        public void Unhighlight()
        {
            this.image.color = Color.white;
        }
    }
}