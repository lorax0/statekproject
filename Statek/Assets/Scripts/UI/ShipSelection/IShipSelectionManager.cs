﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.UI.Menu
{
    public interface IShipSelectionManager
    {
        GameObject ShipSelectionMenu { get; }

        IEnumerable<IShipController> ShipControllers { get; }

        IShip SelectedShip { get; }

        void SubscribeEvents();

        void SelectShip(object sender, EventArgs args);

        bool IsShipSelected();

        void CreateShip();

        void DisableSelectionMenu();

        void EnableSelectionMenu();
    }
}