﻿using UnityEngine;
using System;

namespace Statek.UI.Menu
{
    public interface IShip
    {
        event EventHandler<EventArgs> OnShipSelect;

        GameObject ShipPrefab { get; set; }
        
        void OnShipClick();

        void Highlight();

        void Unhighlight();
    }
}