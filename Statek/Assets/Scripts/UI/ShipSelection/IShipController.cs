﻿namespace Statek.UI.Menu
{
    public interface IShipController
    {
        IShip Ship { get; }
    }
}