﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Statek.UI.Menu
{
    public class ShipController : MonoBehaviour, IShipController, IPointerDownHandler
    {
        public IShip Ship => this.ship;
        [SerializeField] protected Ship ship;

        public void OnPointerDown(PointerEventData eventData)
        {
            this.ship.OnShipClick();
        }
    }
}