﻿using System;
using System.Collections.Generic;
using Statek.Enemies.States;
using UnityEngine;
using Statek.Movement;

namespace Statek.Enemies
{
    [Serializable]
    public class Enemy : IEnemy
    {
        public Transform Transform { get => this.transform; set => this.transform = value; }
        public State State => this.state;
        public IMover Mover => this.mover;
        public float VisibilityDistance => this.visibilityDistance;
        public float AttackRange => this.attackRange;
        public float SearchingTime => this.attackRange;

        [SerializeField] protected Mover mover;
        [SerializeField] protected float visibilityDistance;
        [SerializeField] protected float attackRange;
        [SerializeField] protected float searchingTime;

        protected State state;
        protected Transform transform;

        public void SetStartValue()
        {
            this.mover.Rigidbody = this.transform.GetComponent<Rigidbody2D>();
            this.ChangeState(new SleepState(null));
        }

        public void DoAction() => this.state.DoAction();

        public void ChangeState(State state)
        {
            state.Enemy = this;
            this.state = state;
            Debug.Log(state.ToString());
        }

    }
}
