﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Statek.Enemies.States;

namespace Statek.Enemies
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class EnemyController : MonoBehaviour, IEnemyController
    {
        public IEnemy Enemy => this.enemy;
        [SerializeField] protected Enemy enemy;

        private void Awake()
        {
            this.enemy.Transform = this.transform;
            this.enemy.SetStartValue();
        }

        private void Update()
        {
            this.enemy.DoAction();
        }

    }
}