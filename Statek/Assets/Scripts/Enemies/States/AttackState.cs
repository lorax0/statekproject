﻿using System;
using UnityEngine;
using Statek.Health;

namespace Statek.Enemies.States
{
    [Serializable]
    public class AttackState : State
    {
        public AttackState(IDamageable target)
        {
            this.target = target;
        }

        public override void DoAction()
        {
            //TODO
            this.SetNewState();
        }

        public override void SetNewState()
        {
            bool isInRange = Vector2.Distance(this.enemy.Transform.position, this.target.Transform.position) <= this.enemy.AttackRange;
            if (!isInRange)
            {
                this.enemy.ChangeState(new ChaseState(this.target));
                return;
            }
        }
    }
}
