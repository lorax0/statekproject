﻿using Statek.Health;
using System;
using UnityEngine;

namespace Statek.Enemies.States
{
    [Serializable]
    public class SearchingState : State
    {
        protected float searchingTime;
        public SearchingState(IDamageable target)
        {
            this.target = target;
        }

        public override void DoAction()
        {
            //TODO
            this.searchingTime += Time.deltaTime;
            this.SetNewState();
        }

        public override void SetNewState()
        {
            var findEnemy = this.FindTargetInRange(this.enemy.VisibilityDistance, this.enemy.Transform.position);
            if (findEnemy)
            {
                this.enemy.ChangeState(new ChaseState(this.target));
                return;
            }
            var lostEnemy = this.searchingTime > this.enemy.SearchingTime;
            if (lostEnemy)
            {
                this.enemy.ChangeState(new SleepState(this.target));
                this.enemy.Mover.Break();
                return;
            }
        }
    }
}