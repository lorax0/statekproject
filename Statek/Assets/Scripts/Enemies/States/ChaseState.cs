﻿using Statek.Health;
using Statek.Movement;
using System;
using UnityEngine;

namespace Statek.Enemies.States
{
    [Serializable]
    public class ChaseState : State
    {
        protected IMover mover;
        public ChaseState(IDamageable target)
        {
            this.target = target;
        }

        public override void DoAction()
        {
            this.enemy.Mover.Move(this.target.Transform.position - this.enemy.Transform.position);
            this.SetNewState();
        }

        public override void SetNewState()
        {
            var loseEnemy = !this.FindTargetInRange(this.enemy.VisibilityDistance, this.enemy.Transform.position);
            if (loseEnemy)
            {
                this.enemy.ChangeState(new SearchingState(this.target));
                return;
            }
            var isInRange = Vector2.Distance(this.enemy.Transform.position, this.target.Transform.position) <= this.enemy.AttackRange;
            if (isInRange)
            {
                this.enemy.ChangeState(new AttackState(this.target));
                this.enemy.Mover.Break();
                return;
            }
        }
    }
}
