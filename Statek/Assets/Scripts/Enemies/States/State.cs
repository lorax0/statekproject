﻿using System.Collections.Generic;
using System.Linq;
using Statek.Health;
using UnityEngine;

namespace Statek.Enemies.States
{
    public abstract class State
    {
        public IDamageable Target => this.target;
        public IEnemy Enemy { get => this.enemy; set => this.enemy = value; }

        protected IDamageable target;
        protected IEnemy enemy;

        public abstract void DoAction();

        public abstract void SetNewState();

        public bool FindTargetInRange(float maxDistance, Vector2 currentPosition)
        {
            IEnumerable<IDamageable> enemies = MonoBehaviour.FindObjectsOfType<MonoBehaviour>().OfType<IDamageable>();
            float minDistance = Mathf.Infinity;
            foreach (var enemy in enemies)
            {
                float distance = Vector2.Distance(enemy.Transform.position, currentPosition);
                if(distance > maxDistance)
                {
                    enemies.ToList().Remove(enemy);
                    continue;
                }
                if (distance < minDistance)
                {
                    minDistance = distance;
                }
            }
            IDamageable target = enemies.Where(t => Vector2.Distance(t.Transform.position, currentPosition) == minDistance).FirstOrDefault();
            this.target = target;
            return target != null;
        }
    }
}