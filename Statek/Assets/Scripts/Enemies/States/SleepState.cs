﻿using Statek.Health;
using System;
using UnityEngine;

namespace Statek.Enemies.States
{
    public class SleepState : State
    {
        public SleepState(IDamageable target)
        {
            this.target = target;
        }

        public override void DoAction()
        {
            this.SetNewState();
        }

        public override void SetNewState()
        {
            bool seeEnemy = this.FindTargetInRange(this.enemy.VisibilityDistance, this.enemy.Transform.position);
            if (seeEnemy)
            {
                this.enemy.ChangeState(new ChaseState(this.target));
                return;
            }
        }
    }
}