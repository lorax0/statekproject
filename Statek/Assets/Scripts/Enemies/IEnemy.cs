﻿using UnityEngine;
using Statek.Enemies.States;
using Statek.Movement;

namespace Statek.Enemies
{
    public interface IEnemy
    {
        Transform Transform { get; set; }
        State State { get; }
        IMover Mover { get; }
        float VisibilityDistance { get; }
        float AttackRange { get; }
        float SearchingTime { get; }
        void DoAction();
        void ChangeState(State state);
    }
}