﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Factory<T> : IFactory<T> where T : MonoBehaviour
{
    public T Prefab => this.prefab;

    [SerializeField] protected T prefab;

    public T CreateInstance()
    {
        return MonoBehaviour.Instantiate(this.prefab);
    }
}
