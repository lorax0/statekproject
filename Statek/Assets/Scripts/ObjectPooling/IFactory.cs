﻿using UnityEngine;

public interface IFactory<T> where T : MonoBehaviour
{
    T Prefab { get; }

    T CreateInstance();
}