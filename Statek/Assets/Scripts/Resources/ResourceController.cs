﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Resources
{
    public class ResourceController : MonoBehaviour, IResourceController
    {
        public Resource Resource => this.resource;

        public int Amount => this.amount;

        [SerializeField] protected Resource resource;
        [SerializeField] protected int amount;

        public void AddResource() => this.resource.AddResource(this.amount);
        public void RemoveResource() => this.resource.RemoveResource(this.amount);
    }
}
