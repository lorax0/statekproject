﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Resources
{
    [CreateAssetMenu(fileName = "NewResource", menuName = "Statek/Resource")]
    public class Resource : ScriptableObject, IResource
    {
        public string Name => this.name;

        public string Description => this.description;

        public Sprite Image => this.image;

        public int Amount => this.amount;

        public int MaxAmount { get => this.maxAmount; set => this.maxAmount = value; }

        public event EventHandler<EventArgs> OnResourceTake;

        [SerializeField] protected new string name;
        [SerializeField] protected string description;
        [SerializeField] protected Sprite image;
        [SerializeField] protected int amount;
        [SerializeField] protected int maxAmount;

        public void AddResource(int amount)
        {
            this.amount += amount;
            this.OnResourceTake?.Invoke(this, EventArgs.Empty);
        }

        public void RemoveResource(int amount)
        {
            this.amount -= amount;
            this.OnResourceTake?.Invoke(this, EventArgs.Empty);
        }

        public bool CanRemoveResource(int amount)
        {
            bool isLessThanZero = this.amount - amount < 0;
            if (isLessThanZero)
            {
                return false;
            }
            return true;
        }

        public bool CanAddResource(int amount)
        {
            bool isMoreThanMaxAmount = this.amount + amount > this.maxAmount;
            if (isMoreThanMaxAmount)
            {
                return false;
            }
            return true;
        }
    }
}
