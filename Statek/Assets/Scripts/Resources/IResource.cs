﻿using UnityEngine;
using System;

namespace Statek.Resources
{
    public interface IResource
    {
        event EventHandler<EventArgs> OnResourceTake;
        string Name { get; }
        string Description { get; }
        Sprite Image { get; }
        int Amount { get; }
        int MaxAmount { get; set; }
        void AddResource(int amount);
        void RemoveResource(int amount);
        bool CanRemoveResource(int amount);
        bool CanAddResource(int amount);
    }
}