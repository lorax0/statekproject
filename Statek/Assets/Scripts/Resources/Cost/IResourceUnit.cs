﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Resources
{
    public interface IResourceUnit
    {
        IResource Resource { get; }
        int RequiredAmount { get; }
    }
}