﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Resources
{
    [Serializable]
    public class ResourceUnit : IResourceUnit
    {
        public IResource Resource => this.resource;
        public int RequiredAmount => this.requiredAmount;

        [SerializeField] protected Resource resource;
        [SerializeField] protected int requiredAmount;
    }
}