﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace Statek.Resources
{
    [Serializable]
    public class ResourceView : IResourceView
    {
        public Resource Resource => this.resource;

        public Image Image => this.image;

        public Text Text => this.text;

        [SerializeField] protected Resource resource;
        [SerializeField] protected Image image;
        [SerializeField] protected Text text;

        public void Initialize()
        {
            this.UpdateResourceAmountImage(this, new EventArgs { });
            this.image.sprite = this.resource.Image;
            this.resource.OnResourceTake += this.UpdateResourceAmountImage;
        }

        public void UpdateResourceAmountImage(object sender, EventArgs args)
        {
            this.text.text = this.resource.Amount.ToString();
        }

    }
}