﻿using System;
using UnityEngine.UI;

namespace Statek.Resources
{
    public interface IResourceView
    {
        Resource Resource { get; }
        Image Image { get; }
        Text Text { get; }
        void UpdateResourceAmountImage(object sender, EventArgs args);
    }
}