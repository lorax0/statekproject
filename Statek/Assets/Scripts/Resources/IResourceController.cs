﻿namespace Statek.Resources
{
    public interface IResourceController
    {
        Resource Resource { get; }
        int Amount { get; }
    }
}