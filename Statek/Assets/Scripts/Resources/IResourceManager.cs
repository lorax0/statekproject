﻿using System.Collections.Generic;

namespace Statek.Resources
{
    public interface IResourceManager
    {
        IEnumerable<IResourceView> ResourceViews { get; }
    }
}