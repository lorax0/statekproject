﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Resources
{
    public class ResourceManager : MonoBehaviour, IResourceManager
    {
        public IEnumerable<IResourceView> ResourceViews => this.resourceViews;

        [SerializeField] protected List<ResourceView> resourceViews;
        
        private void Awake()
        {
            foreach(var resource in this.resourceViews)
            {
                resource.Initialize();
            }
        }
    }
}
