﻿using UnityEngine;
using System.Collections.Generic;

namespace Statek.Particles
{
    public interface IParticleSystem
    {
        IEnumerable<ParticleSystem> Particles { get; }
        
        void SetParticlesActivity(bool value);

        void Activate();

        void Deactivate();
    }
}
