﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Statek.Particles
{
    [Serializable]
    public class MovementParticleSystem : IParticleSystem
    {
        public IEnumerable<ParticleSystem> Particles => this.particles;
        [SerializeField] protected List<ParticleSystem> particles;
        protected bool isPlaying;

        public void SetParticlesActivity(bool isMoving)
        {
            if (this.isPlaying == false && isMoving)
            {
                this.Activate();
            }
            else if(this.isPlaying == true && !isMoving)
            {
                this.Deactivate();
            }
        }

        public void Activate()
        {
            foreach (var particle in this.particles)
            {
                particle.Play();
            }
            this.isPlaying = true;
        }

        public void Deactivate()
        {
            foreach (var particle in this.particles)
            {
                particle.Stop();
            }
            this.isPlaying = false;
        }

    }
}
