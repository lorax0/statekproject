﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Statek.Player.Inventory
{
    public class InventoryController : MonoBehaviour, IInventoryController
    {
        public IInventory Inventory => this.inventory;
        
        [SerializeField] protected Inventory inventory;
        protected GameObject inventoryObject;

        private void Awake()
        {
            this.inventory.SetStartValue();
            this.inventory.CreateSlots(this.inventory.SlotsAmount.Statistic);
            this.inventoryObject = this.inventory.Transform.gameObject;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                this.ToogleInventory();
            }
        }

        private void ToogleInventory()
        {
            this.inventoryObject.SetActive(!this.inventoryObject.activeSelf);
        }
    }
}
