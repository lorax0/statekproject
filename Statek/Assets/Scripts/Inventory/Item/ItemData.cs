﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Player.Inventory
{
    [CreateAssetMenu(fileName = "ItemData", menuName = "Statek/Inventory/ItemData")]
    public class ItemData : ScriptableObject, IItemData
    {
        public int MaxItemsAmount => this.maxItemsAmount;

        public GameObject Prefab => this.prefab;

        public string Description => this.description;

        public string Name => this.name;

        public Sprite Sprite => this.sprite;


        [SerializeField] protected GameObject prefab;
        [SerializeField] protected string description;
        [SerializeField] protected new string name;
        [SerializeField] protected Sprite sprite;
        [SerializeField] protected int maxItemsAmount;
    }
}
