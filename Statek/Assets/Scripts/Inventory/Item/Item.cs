﻿using UnityEngine;
using System;
using System.Collections;

namespace Statek.Player.Inventory
{
    [Serializable]
    public class Item : IItem
    {
        public IItemData ItemData => this.itemData;

        public virtual int MaxItemsAmount => this.itemData.MaxItemsAmount;

        public virtual GameObject Prefab => this.itemData.Prefab;

        public virtual string Description => this.itemData.Description;

        public virtual string Name => this.itemData.Name;

        public virtual Sprite Sprite => this.itemData.Sprite;

        [SerializeField] protected ItemData itemData;
    }
}