﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Player.Inventory
{
    public class ItemController : MonoBehaviour, IItemController
    {
        public IItem Item => this.item;
        [SerializeField] Item item = null;

        protected Inventory inventory;

        private void Start()
        {
            this.inventory = Inventory.Instance;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.tag != "Player")
            {
                return;
            }
            this.inventory?.AddItem(this.item);
        }

    }
}
