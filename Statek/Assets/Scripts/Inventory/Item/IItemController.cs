﻿namespace Statek.Player.Inventory
{
    public interface IItemController
    {
        IItem Item { get; }
    }
}