﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Statek.Player.Inventory
{
    public interface IItem
    {
        IItemData ItemData { get; }
        int MaxItemsAmount { get; }
        Sprite Sprite { get; }
        string Name { get; }
        string Description { get; }
        GameObject Prefab { get; }
    }
}
