﻿using UnityEngine;

namespace Statek.Player.Inventory
{
    public interface IItemData
    {
        int MaxItemsAmount { get; }
        GameObject Prefab { get; }
        string Description { get; }
        string Name { get; }
        Sprite Sprite { get; }
    }
}