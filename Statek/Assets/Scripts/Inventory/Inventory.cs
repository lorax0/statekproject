﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Statek.Research;

namespace Statek.Player.Inventory
{
    [Serializable]
    public class Inventory : IInventory
    {
        public static Inventory Instance => instance;
        protected static Inventory instance;

        public Transform Transform => this.transform;
        public GameObject SlotObject => this.slotObject;
        public IResearchableStatistic SlotsAmount => this.slotsAmount;
        public IEnumerable<ISlot> Slots { get => this.slots; set => this.slots = (List<Slot>) value; }

        [SerializeField] protected ResearchableStatistic slotsAmount;
        [SerializeField] protected GameObject slotObject;
        [SerializeField] protected Transform transform;

        protected List<Slot> slots = new List<Slot>();

        public void SetStartValue()
        {
            this.SetSingleton();
            this.slotsAmount.OnStatisticValueChange += this.UpdateSlotsAmount;
        }

        public void SetSingleton()
        {
            if (instance == null)
            {
                instance = (Inventory) MonoBehaviour.FindObjectOfType<InventoryController>().Inventory;
                if (instance == null)
                {
                    Debug.LogWarning("An instance of " + typeof(InventoryController) + " is needed in the scene, but there is none.");
                }
            }
        }

        public void AddItem(IItem item)
        {
            foreach(var slot in this.slots)
            {
                if (slot.CanAddItem())
                {
                    slot.AddItem(item);
                    return;
                }
            }
        }

        public void CreateSlots(float amount)
        {
            for (int i = 0; i < amount; i++)
            {
                var slotController = MonoBehaviour.Instantiate(slotObject, this.transform).GetComponent<ISlotController>();
                var slot = (Slot) slotController.Slot;
                this.slots.Add(slot);
            }
        }

        public void DestroySlots()
        {
            foreach(var slot in slots)
            {
                MonoBehaviour.Destroy(slot.Transform);
            }
            this.slots.Clear();
        }

        public void UpdateSlotsAmount(object sender, EventArgs args)
        {
            this.DestroySlots();
            this.CreateSlots(this.slotsAmount.Statistic);
        }
    }
}
