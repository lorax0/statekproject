﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Statek.Research;

namespace Statek.Player.Inventory
{
    public interface IInventory
    {
        Transform Transform { get; }

        GameObject SlotObject { get; }

        IEnumerable<ISlot> Slots { get; set; }

        IResearchableStatistic SlotsAmount { get; }

        void SetStartValue();

        void AddItem(IItem item);

        void CreateSlots(float amount);

        void DestroySlots();

        void UpdateSlotsAmount(object sender, EventArgs args);
    }
}
