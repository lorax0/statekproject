﻿using System;
using System.Collections.Generic;

namespace Statek.Player.Inventory
{
    public interface IInventoryController
    {
        IInventory Inventory { get; }
    }
}
