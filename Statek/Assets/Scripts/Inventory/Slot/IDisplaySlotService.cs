﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Statek.Player.Inventory
{
    public interface IDisplaySlotService
    {
        Image ItemImage { set; }
        Text AmountText { set; }
        void SetSlotImage(Sprite sprite);
        void RemoveImage();
        void SetAmountText(int amount);
        void ClearAmountText();
    }
}
