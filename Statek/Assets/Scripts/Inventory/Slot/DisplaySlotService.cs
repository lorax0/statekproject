﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Statek.Player.Inventory
{
    [Serializable]
    public class DisplaySlotService : IDisplaySlotService
    {
        public Image ItemImage { set => this.itemImage = value; }
        public Text AmountText { set => this.amountText = value; }

        [SerializeField] protected Image itemImage;
        [SerializeField] protected Text amountText;

        public void SetSlotImage(Sprite sprite)
        {
            this.itemImage.sprite = sprite;
        }

        public void RemoveImage()
        {
            this.itemImage.sprite = null;
        }

        public void SetAmountText(int amount)
        {
            this.amountText.text = amount.ToString();
        }

        public void ClearAmountText()
        {
            this.amountText.text = null;
        }
    }
}
