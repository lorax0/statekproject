﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Statek.Player.Inventory
{
    [Serializable]
    public class Slot : ISlot
    {
        public IItem Item => this.item;
        public IDisplaySlotService DisplaySlotService { get => this.displaySlotService; set => this.displaySlotService = (DisplaySlotService)value; }
        public Transform Transform => this.transform;


        [SerializeField] protected DisplaySlotService displaySlotService;
        [SerializeField] protected Transform transform;
        protected Item item;
        protected int itemsAmount;

        public void AddItem(IItem item)
        {
            this.item = (Item) item;
            this.itemsAmount++;
            this.displaySlotService?.SetSlotImage(item.Sprite);
            this.displaySlotService?.SetAmountText(this.itemsAmount);
        }

        public void RemoveItem()
        {
            this.itemsAmount--;
            if (this.itemsAmount == 0)
            {
                this.ClearSlot();
                return;
            }
            this.displaySlotService?.SetAmountText(this.itemsAmount);
        }

        public void ClearSlot()
        {
            this.item = null;
            this.itemsAmount = 0;
            this.displaySlotService?.RemoveImage();
            this.displaySlotService?.ClearAmountText();
        }

        public bool CanAddItem()
        {
            if (this.IsEmpty())
            {
                return true;
            }
            if (this.IsNotFull())
            {
                return true;
            }
            return false;
        }

        public bool IsEmpty() => this.GetItem() == null;

        public bool IsNotFull() => this.GetItem()?.MaxItemsAmount > this.itemsAmount;

        public IItem GetItem() => this.item;
    }
}
