﻿using System;
using System.Collections.Generic;

namespace Statek.Player.Inventory
{
    public interface ISlotController
    {
        ISlot Slot { get; }
    }
}
