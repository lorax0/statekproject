﻿using System;
using System.Collections.Generic;

namespace Statek.Player.Inventory
{
    public interface ISlot
    {
        IItem Item { get; }
        IDisplaySlotService DisplaySlotService { get; }
        void AddItem(IItem item);
        void RemoveItem();
        bool IsEmpty();
        IItem GetItem();
    }
}
