﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Statek.Player.Inventory
{
    public class SlotController : MonoBehaviour, ISlotController
    {
        public ISlot Slot => this.slot;

        [SerializeField] protected Slot slot;
    }
}
