# Statek

## Unity version used

2019.4.3f1

## Statki 
- Lekki my�liwiec - nastawiony na lasery, do�� szybki. Lata minimalnie szybciej poza combatem. 
- Ci�ki my�liwiec - nastawiony na lasery, wolniejszy, wi�cej pancerza.
- Kr��ownik - nastawiony na rakiety, wolniejszy, wi�cej pancerza.
- Pancernik - defensywny, nastawiony na samonapraw�, wolniejszy, wi�cej pancerza. Jak dodamy do gry aggro, ten b�dzie bazowo generowa� aggro 3x ze swoich obra�e�. Pulla statki pve z wi�kszego zasi�gu. Pasywnie otrzymuje oko�o 30% wi�kszy healing zar�wno ze swoich, jak i sojuszniczych system�w. 
- Support - nastawiony na napraw� innych statk�w, szybki, ma�o pancerza. Lata minimalnie szybciej poza combatem. 
- UFO kr��ek multitype - totalne u�rednienie, nie b�dzie mia� �adnej dobrej pasywki, ale b�dzie mia� odblokowane wszystkie sloty. 
- Ma�y transporter - du�a �adowno��, do�� szybki, mniej pancerza a wi�ksze tempo naprawy. 
- Du�y transporter - bardzo du�a �adowno��, ale wolniejszy ni� ma�y transporter. Wi�cej �ycia, mniej leczenia i mniejsze obra�enia ni� ma�y transporter. 
- Recykler - statek z du�� �adowno�ci� i szybkim zbieractwem, niezbyt szybki, niezbyt waleczny, raczej du�o �ycia. Pasywnie pokazuje na minimapie pola zniszcze� w zasi�gu 3-4x wi�kszym, ni� normalnemu statkowi. 
- Statek szpiegowski - Male�ki, z generatorem stealtha poza combatem, pada na kilka klep, lata bardzo szybko poza combatem, male�ka �adowno��. B�dzie przydatny g��wnie do rzeczy typu szukanie bossa czy innych wa�nych rzeczy na mapie, ale gracz z dobrymi technologiami b�dzie te� m�g� u�ywa� go do napierdalania s�abych przeciwnik�w, �eby co� szybko wygrindowa�, np jaki� achievement typu �zabij 10k przeciwnik�w�. Mo�na przemy�le�, czy nie powinni�my mu da� mo�liwo�ci generowania stealtha te� dla innych statk�w, np tylko poza combatem lub tylko poza combatem i kiedy pozostaj� bez ruchu, w celu robienia trap�w w pvp. Ale to kiedy�, jak si� zrobi pvp i troch� potestuje. 
## Sloty
- Sloty na uzbrojenie, nap�dy itd b�d� dzia�a� w nast�puj�cy spos�b. Ka�dy statek b�dzie mia� kilka slot�w, np 5, 6 czy 7. Lepsze statki mog� mie� wi�cej slot�w. Dany statek b�dzie mo�na budowa� wg danego schematu, bo ka�dy slot b�dzie w mniejszym lub wi�kszym stopniu zablokowany.
- Slot uniwersalny - mo�na wsadzi� cokolwiek
- Slot ofensywny - mo�na wsadzi� lasery, rakiety, jakie� aury AOE itp
- Slot na lasery - mo�na wsadzi� tylko lasery
- Slot na rakiety - mo�na wsadzi� tylko rakiety 
- Slot na aury ofensywne - mo�na wsadzi� tylko aury ofensywne 
- Slot defensywny - mo�na wsadzi� dowolny system defensywny dla siebie lub sojusznika
- Slot defensywny dla siebie - mo�na wsadzi� system defensywny tylko dla siebie
- Slot defensywny wspieraj�cy - mo�na wsadzi� system defensywny tylko dla ca�ego party
- Slot naprawczy - dowolny system naprawczy dla siebie lub sojusznik�w
- Slot naprawczy dla siebie - mo�na wsadzi� system naprawczy tylko dla siebie
- Slot naprawczy wspieraj�cy - mo�na wsadzi� system naprawczy tylko dla ca�ego party
- Slot nap�dowy - mo�na wsadzi� dowolny system nap�du
- Slot magazynowy - mo�na wsadzi� �adowni�, kt�ra zwi�kszy pojemno�� statku
Mo�emy zrobi� statki bardziej uniwersalne, w kt�rych wszystkie lub prawie wszystkie sloty s� odblokowane, ale jest 1 slot mniej ni� w innych statkach. Co� za co�. 
## Statystyki
- Pancerz aktualny/maksymalny - punkty �ycia.
- Os�ona aktualna/maksymalna - trzeba j� przebi� przed uderzeniem w pancerz, odnawia si� szybciej poza combatem. 
- Regeneracja os�ony w combacie.
- Regeneracja os�ony poza combatem.
- Crit chance, bazowo 0%. 
- Aktualna pr�dko�� - pr�dko�� z jak� statek porusza si� w danej chwili. 
- Bazowa pr�dko�� w combacie.
- Bazowa pr�dko�� poza combatem. 
- Pr�dko�� (przepustowo��) �adowania surowc�w na statek.
- Pojemno�� magazynu.
## Uzbrojenie
- Ofensywne
	- Lasery - szybkie ataki single target. 
		- Niebieski - standardowy laser, x obra�e� z ka�dym atakiem.
		- Czerwony - laser, kt�ry ma mniejsze bazowe obra�enia (powiedzmy 0.8 x), ale zadaje obra�enia over time, np 0.5x przez p� minuty, dzi�ki czemu gracz ma znacznie wi�ksze obra�enia w przeciwnika, kt�ry ma du�o punkt�w �ycia, kosztem zmniejszonych obra�e� w przeciwnik�w, kt�rzy maj� mniej punkt�w �ycia. 
		- Zielony - zadaje zwi�kszone obra�enia, je�li atakuje stale jeden cel, powiedzmy od 0.5x narasta do 3x w ci�gu 90 sekund. Ten punkt jest do przedyskutowania, mo�e macie jaki� lepszy pomys�. 
		- B��kitny - laser zadaje 0.6x obra�e�. Pierwsze uderzenie nak�ada slowa 20%, ka�de nast�pne dok�ada 1% a� do 40%. Po na�o�eniu maks slowa, mamy 10% szans na obra�enia wi�ksze o 100% (crit czy co�), co daje nam przeci�tne obra�enia 0.66x, ale gracza b�dzie jara�, jak zobaczy hita x2. By� mo�e da to graczom mo�liwo�� robienia si� w og�le pod crita. 
	- Rakiety - powolne, pot�ne ataki AOE o �rednim polu ra�enia. Rakieta leci znacznie wolniej ni� laser, dolatuje do przeciwnika i eksploduje, przy czym zadaje obra�enia wszystkim przeciwnikom w zasi�gu. Obra�enia s� wyra�nie wi�ksze ni� obra�enia lasera, ale ze wzgl�du na powolne prze�adowanie op�aca si� dopiero przy trzech przeciwnikach, kt�rych trafi. Rakiety spawnuj� si� wok� statku r�wnie� wtedy, kiedy nie ma targetu. Maksymalnie 3 rakiety na raz mog� by� aktywne nie maj�c targetu, pod��aj� wtedy blisko statku w�a�ciciela i czekaj� na target. Po znalezieniu targetu, wszystkie na raz wlatuj� we wroga. 
		- Aura AOE - aura na obra�enia wok� statku, nielimitowana ilo�� atakowanych przeciwnik�w, obra�enia 3x ni�sze ni� lasery, podobna cz�stotliwo�� ataku i minimalnie wi�kszy zasi�g. Animacja mo�e przypomina� co� w rodzaju piorun�w uderzaj�cych jednocze�nie w ka�dego przeciwnika w zasi�gu. 
		tu zn�w b�dziemy mogli wyr�ni� inne aury z czasem, np aur� zmniejszaj�c� tempo naprawy przeciwnik�w, aur� daj�c� slowa, aur� o narastaj�cych obra�eniach czy mo�e aur�, kt�ra generuje du�e aggro i powoduje, �e statki pve atakuj� gracza, kt�ry jej u�ywa
- Defensywne
	- Zwi�kszaj�ce pancerz - zwi�kszaj� MAKSYMALNE punkty pancerza statku o x. 
	- Zwi�kszaj�ce os�on� - j/w.
	- Aura generuj�ca os�ony - dzia�a na wszystkich sojusznik�w w okolicy, generuje os�on� o wielko�ci x na sekund�, maksymalnie do 20x. Je�li sojusznik opu�ci zasi�g aury, dodatkowe punkty przestaj� by� generowane, a wygenerowane wcze�niej znikaj� po 10 sekundach. 
- Lecz�ce
	- AOE - leczy wszystkie statki dooko�a w zasi�gu niewiele wi�kszym ni� laser, z podobn� cz�stotliwo�ci�, za ok 25% obra�e� lasera. 
	- Self HoT - leczy statek w�a�ciciela z cz�stotliwo�ci� podobn� do lasera za jakie� 75% jego obra�e�.
	- HoT z prio na cel z najni�szym poziomem pancerza - j/w, ale leczy sojusznika z najni�szym poziomem pancerza. 
	- HoT z prio na siebie do 100%, potem j/w.
- Nap�dy
- Ud�wig
	- �adownia - zwi�ksza maksymaln� �adowno�� statku.
	- System �aduj�cy - zwi�ksza tempo za�adunku.
## Surowce
- Z�oto
- Z�om
- Paliwo czy co� takiego 
## Technologia
- Technologia badawcza - przyspiesza rozw�j innych technologii, konkretnie mno�y czas bada� wst�pnie przez 0.8n. 
- Technologia szybkiej budowy - przyspiesza budowanie budynk�w, j/w. 
- Badania efektywno�ci pancerza - zwi�ksza pancerz statk�w o %. 
- Badania efektywno�ci os�ony - zwi�ksza ilo�� punkt�w os�ony o %. 
- Badania generacji os�ony - zwi�ksza regeneracj� os�ony o %. 
- Technologia nap�dowa - zwi�ksza bazow� pr�dko�� statku o %. 
- Technologia laserowa - zwi�ksza obra�enia od laser�w o % (w przysz�o�ci doda si� tak�e technologie zwi�zane z pr�dko�ci� strza�u lasera i osobn� technologi� do ka�dego typu lasera).
- Technologia bombowa (jak wy�ej, tylko pod rakiety).
Wszystkie technologie wymagaj� x surowc�w maj� wymagania budynkowe i/lub technologiczne, w najlepszym dla gracza wypadku jedyne wymaganie to laboratorium na 1 poziomie, ale mog� te� by� wymagane r�ne poziomy innych technologii lub budynk�w, np technologia zielonego lasera mo�e wymaga� technologii laserowej na 3 poziomie i warsztatu na 2. 
## Budynki
- Kopalnia z�ota - wydobywa z�oto, wydobycie 200 golda na godzin� wygl�da nast�puj�co� 3600/200 = 18, co 18 sekund przybywa jedna sztuka z�ota, kiedy jeste�my w bazie. Poza gr� bierzemy czas, kiedy u�ytkownik ostatnio by� online i sprawdzamy ile surowca powinno si� w tym czasie wydoby�. Czas offline w sekundach dzielimy przez 3600 i mno�ymy przez wydobycie godzinne, np 10000/3600*200 = 555 z�ota. MEGA wa�ne jest, �eby czas pobiera� si� z serwera, �eby gracz nie m�g� sobie przestawia� zegarka w niesko�czono��. Je�li nie da si� pobra� czasu z serwera, to niech budynki �wiec� si� na czerwono, a gracz dostanie powiadomienie, �e ma w��czy� neta. 
- Kopalnia metalu - wydobywa z�om, j/w.
- Elektrownia - ka�dy nast�pny poziom daje x energii, z kolei ka�dy budynek poza elektrowni� i spichlerzem potrzebuje energii do pracy (w praktyce - do bycia wybudowanym). Je�li elektrownia daje 100 energii, kopalnia z�ota zu�ywa 30, kopalnia metalu 40, a laboratorium 25, to mamy 95/100 energii zu�yte i nie postawimy �adnego budynku, kt�rego budowa/upgrade wymaga wi�cej ni� 5 energii. 
- Spichlerz/magazyn - umo�liwia trzymanie surowc�w na planecie czy w bazie. Je�li mamy spichlerz umo�liwiaj�cy trzymanie x surowca, to mamy osobne x miejsca na ka�dy surowiec. Je�li x miejsca zostanie zape�nione, to PASYWNY przyrost tego surowca zostaje zatrzymany i surowce w bazie pokazuj� nam si� w krwistym czerwonym kolorze, ale nadal mo�emy magazynowa� zebrane w boju surowce a� do 100% przepe�nienia magazynu, czyli do 2x, wtedy kolor surowca zmienia si� na ciemny czerwony, a przy pr�bie roz�adowania statku dostajemy info, �e surowce zostan� skasowane, bo magazyn jest przeci��ony. 
- Laboratorium badawcze - ka�dy poziom daje 10 badaczy, co 3 poziomy (1, 4, 7 itd) gracz dostaje slota na nowe badanie, 3 sloty = do trzech bada� prowadzonych jednocze�nie. Badaczy mo�na rozdzieli� mi�dzy badania w dowolnej proporcji, po przydzieleniu innej liczby badaczy, czas jest kalkulowany ponownie. 
- Centrum badawczo-rozwojowe. Ten budynek dodamy z du�ym op�nieniem. Dzi�ki niemu b�dzie mo�na wsp�lnie z przyjaci�mi bada� bardziej skomplikowane badania przy u�yciu badaczy z laboratorium. 
- Warsztat - ka�dy nast�pny poziom warsztatu skraca tempo budowy nowych komponent�w do statk�w czy samych statk�w. Dodatkowo co x poziom�w warsztatu dostajemy kolejny slot w kolejce, jak w laboratorium. 
Bardzo wa�ne jest, �e do budowy niekt�rych budynk�w b�dzie trzeba spe�ni� wi�cej, ni� 1 z poni�szych kryteri�w:
- odpowiednia liczba surowc�w
- odpowiednio du�o wolnej energii
spe�nione wymagania odno�nie poziomu innego budynku, np centrum badawczo-rozwojowe mo�e wymaga� laboratorium na co najmniej 8 poziomie

## Mapki, przeciwnicy
- Mapy b�d� generowane losowo.
- Gracz b�dzie wiedzia� jakie s� procentowe szanse na spotkanie na danej mapie przeciwnika danego typu.
- Na ka�dej mapie b�d� losowani przeciwnicy z badaniami, kt�rych zakres b�dzie graczowi znany, np 3-5, 6-9 itd.
- Na ka�dej mapie b�d� si� znajdowa� przeciwnicy elitarni, poziomy uzbrojenia i technologii b�d� mieli o 2 wy�sze ni� reszta, czyli je�li na danej mapie zakres wynosi 3-5, to elitarni b�d� mieli 5-7.
- Bossowie b�d� mieli wszystkie poziomy o 2 wy�sze ni� g�rna granica zakresu danej mapy, czyli je�li zakres danej mapy to 3-5, boss b�dzie mia� wszystko na 7.
## Gameplay
System dropu schemat�w uzbrojenia. Tego jeszcze do ko�ca nie przemy�la�em, w ka�dym razie chc�, �eby to by� totalny grind. Chcesz rozwin�� swoje lasery? Musisz znale�� miejsce na mapie, gdzie jest du�o przeciwnik�w, kt�rzy napierdalaj� laserami i grindowa� ich setkami czy tysi�cami. Raz na kilku czy kilkunastu przeciwnik�w leci kawa�ek schematu i mo�na to jako� mega upro�ci�, �e 10 kawa�k�w daje mo�liwo�� rozwini�cia lasera na poziom 2, 100 na poziom 3, 1000 na poziom 4 itp. Ewentualnie zamiast pot�g 10 wzi�� kolejne pot�gi 7. Chcesz mie� lepszy nap�d? Znajdujesz statki, z kt�rych cz�sto leci nap�d. 
Tytu�y. Dajmy na to, �e nasz nick to FatNigga. Zajebali�my 50 000 statk�w, wi�c dostajemy tytu� �(nick) the Destroyer�. Je�li zaznaczymy sobie w menu tytu��w, �e chcemy u�ywa� danego tytu�u, od tej pory b�dziemy podpisani FatNigga the Destroyer. Oczywi�cie tytu� mo�na w dowolnej chwili zmieni� na ka�dy inny, kt�ry odblokowali�my. Na pewno chcemy tytu��w typu �Alpha/Beta Tester� dla ludzi, kt�rzy zaloguj� si� w alfie lub becie, czy �Founder� dla ludzi, kt�rzy wp�ac� x hajsu w czasie alfy lub bety i wspomog� tym rozw�j gry. 
- Walki dru�ynowe do 5 graczy w dru�ynie, co� na zasadzie diablo 3. 
- Szukasz pokoju, znajdujesz pok�j, napierdalasz ile chcesz, opuszczasz kiedy chcesz. Mo�esz te� ustawi� swoj� gr� jako publiczn�, wtedy stajesz si� hostem i ka�dy mo�e do��czy�. 
- Gry prywatne czy co�, tylko dla znajomych. 
- Przeciwnicy s� tym silniejsi, im wi�cej graczy w dru�ynie, ale bez przesady, �eby op�aca�o si� gra� w kilku. System mo�na te� zajeba� z diablo 3, nie pami�tam jak dok�adnie to tam dzia�a�o, ale to jest do dogadania. 
- Wi�cej graczy, wi�cej dropu.
##Achievementy
Zrobimy ca�e menu takie jak w wowie i zrobimy z tego jedn� z g��wnych zalet tej gry. Pocz�tkowo nie b�dzie �adnych kategorii, ale jak dodamy dziesi�tki r�nych achi, to zrobimy menu rozwijane z lewej strony z kategoriami:
- Summary: podgl�d ostatnich 5 zdobytych achi, rankingu (jak du�o graczy ma wi�cej achi od nas itp), jakie� paski ile na ile achi mamy w ka�dej z kategorii. Mo�na znale�� podobne w google po wpisaniu wow achievements i przej�ciu do grafiki. 
- General: achievementy niepasuj�ce do innych kategorii.
- Quests: achi za questy, np �zr�b 50 r�nych quest�w� albo �zr�b jakiego� mega trudnego questa�.
- Exploration: achi zwi�zane z eksploracj�, np �przele� ��cznie x metr�w�, �zwied� 50 r�nych galaktyk� itp. 
- Pvp: np �zajeb 50 przeciwnik�w� albo �zabij 10 przeciwnik�w z rz�du nie daj�c si� ujeba�.
- Pve: co� jak to wy�ej, tylko dla pve.
- Zbieractwo itp (brakuje mi nazwy): achi typu �zbierz 50 r�nych p�l zniszcze�, czy �znajd� pole zniszcze� o ��cznej zasobno�ci wi�kszej ni� x�. Tu wchodz� tylko achi za rzeczy znalezione losowo w galaktykach, co� jak ro�linki, ska�ki czy inne zasoby w innych grach. 
- Kooperacja: achi za rozjebanie r�nych boss�w czy innych przeciwnik�w w instancjonowanych mapkach generowanych np dla 5, 10 czy 20 znajomych w party. 
- Kolekcje: np �znajd� 500 cz�ci schematu lasera� czy �odblokuj jaki� tam statek�.
- Eventy: np �zr�b wszystkie questy w evencie sylwestrowym� czy �odblokuj �wi�teczn� sk�rk� dla dowolnego statku�.
Za achievementy mo�na dosta� r�ne nagrody, np:
- bonusy do obra�e� czy innych statystyk na sta�e (g��wnie za achievementy zwi�zane z pvp i pve)
- tytu�y wy�wietlane przy nicku
- bonus +1 do jakiego� badania na sta�e, bez ponoszenia koszt�w podniesienia tego badania o 1
- mo�e dost�p do jakiego� badania, do kt�rego normalnie si� dost�pu nie ma